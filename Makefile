
IMGNAME=detect-and-drink


docker-run: docker-build
	docker run -p 8080:80 $(IMGNAME)

deploy:
	docker run -d --privileged --device /dev/snd -v `pwd`/sweet-tunes:/sweet-tunes:ro --name $(IMGNAME) -p 0.0.0.0:80:80 $(IMGNAME)


docker-build:
	docker build . -t $(IMGNAME)
