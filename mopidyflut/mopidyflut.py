import socket
from time import sleep
from PIL import Image, ImageDraw, ImageFont
import numpy as np
from tqdm import tqdm
from mopidyapi import MopidyAPI
import multiprocessing

lineOneFont = ImageFont.truetype("terminus.ttf", 9)
lineTwoFont = ImageFont.truetype("terminus.ttf", 9)

mopidy_addr = "151.216.10.50"
PIXELFLUT_IMPLEMENTS_SIZE = False

m = MopidyAPI(host=mopidy_addr)
print("mopidy connected")

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ip = "100.68.0.3"

sock.connect((ip, 1337))
if PIXELFLUT_IMPLEMENTS_SIZE:
    sock.send(b"SIZE\n")
    recv_size = sock.recv(100)
    SIZE = [int(i) for i in recv_size.decode().split('\n')[0].split(' ')[1:]]
else:
    SIZE = (32, 16)

def calc_pretender(obj):
    (lineOneOffset, lineTwoOffset) = obj
    img = Image.new("RGB", SIZE, "#000005")
    draw = ImageDraw.Draw(img)
    draw.text((lineOneOffset, -3), lineOne, "yellow", font=lineOneFont)
    draw.text((lineTwoOffset, 6), lineTwo, "yellow", font=lineTwoFont)
    frame = set()
    for x in range(img.size[0]):
        for y in range(img.size[1]):
            pixel = img.getpixel((x, y))
            frame.add((x, y, pixel))
    return frame

oldLineOne = ""
oldLineTwo = ""
yOffset = 0

while True:
    track = m.playback.get_current_track()
    lineOne = track.name
    lineTwo = track.artists[0].name

    if lineOne != oldLineOne or lineTwo != oldLineTwo:
        oldLineOne = lineOne
        oldLineTwo = lineTwo
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, 1337))
        i = 0
        frames = []
        lineOneSize = lineOneFont.getsize(lineOne)
        lineTwoSize = lineTwoFont.getsize(lineTwo)
        lineOneOffsets = list(reversed(list(range(-lineOneSize[0], SIZE[0]))))
        lineTwoOffsets = list(reversed(list(range(-lineTwoSize[0], SIZE[0]))))
        if lineOneSize[0] > lineTwoSize[0]:
            print("Line one go brr!")
            offsets = [(lineOneOffsets[int(i * (len(lineOneOffsets) / len(lineTwoOffsets)))], offset) for i, offset in enumerate(lineTwoOffsets)]
        else:
            print("Line two go brr!")
            offsets = [(offset, lineTwoOffsets[int(i * (len(lineTwoOffsets) / len(lineOneOffsets)))]) for i, offset in enumerate(lineOneOffsets)]
        
        with multiprocessing.Pool(multiprocessing.cpu_count()) as pool:
            frames = [frame for frame in tqdm(pool.imap(calc_pretender, offsets), total=len(offsets))]

        frames.append(frames[0])

        prerenders = []
        for i in tqdm(range(1, len(frames))):
            prerender = []
            prev_frame = frames[i - 1]
            frame = frames[i]
            changed_pixels = frame - prev_frame
            for k, c in enumerate(frame):
                # y = k // img.size[0]
                # x = k % img.size[1]
                x = c[0]
                y = c[1]
                pixel = c[2]
                color = bytes(pixel).hex().upper()
                prerender.append(f"PX {x} {y+yOffset} {color}")

            prerenders.append(("\n".join(prerender) + '\n').encode())

    for prerender in prerenders:
        sleep(0.20)
        sock.send(prerender)
        # open("frames", "ab").write(prerender)
