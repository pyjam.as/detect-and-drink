# std lib
from typing import List, Dict, Tuple
from datetime import datetime
from subprocess import check_output
from functools import lru_cache
import os
import random
import threading
import pyttsx3

# deps
#import flask
import flask
import names
import requests
from loguru import logger
from subprocess import check_output

# app imports
from datatypes import Customer, Ip

app = flask.Flask(__name__)

cad = None  # Try to import PiFace (only prod).
try:
    import pifacecad

    cad = pifacecad.PiFaceCAD()
    cad.lcd.backlight_on()
    cad.lcd.blink_off()
    cad.lcd.cursor_off()
except ModuleNotFoundError:
    pass

sweetTunes = "/sweet-tunes"

countryLookupTable = {
    "DK": " the viking",
    "US": " NSA",
    "NL": " high AF",
    "DE": ", PROST!",
    "FR": " baguette",
    "IT": ", mamamia!",
}
# Make sure all keys above are not too big for display
assert all(len(val) <= 11 for val in countryLookupTable.values())

drunks = 0
customers: Dict[Ip, Customer] = {}


@app.route("/tts")
def tts():
    engine = pyttsx3.init()
    engine.say("Hello, World!")
    engine.runAndWait()


@app.route("/")
def webServer():
    try:
        name, n, pronoun = getDrinks(flask.request.remote_addr)
        country = getIpInfo(flask.request.remote_addr)
        logger.info(
            name.ljust(16)
            + "| "
            + str(n).ljust(8)
            + "| "
            + flask.request.remote_addr.ljust(16)
            + "| 0"
        )
        # Play localized sound with default youve got mail
        country_tune_path = f"{sweetTunes}/{country.lower()}.wav"
        spam_notice = ""
        if isSpam(flask.request.remote_addr):
            spam_notice = "<p> Duuuude, stop spamming us </p>"
        else:
            if os.path.isfile(country_tune_path):
                play([country_tune_path])
            else:
                play(
                    [
                        f"{sweetTunes}/youve-got-mail.wav",
                        f"{sweetTunes}/countries/from{country}.mp3",
                    ]
                )
            # update timestamp for customer
            customers[flask.request.remote_addr].time = datetime.now()

        return (
            """
        <h1> <a href="http://pyjam.as"> Pyjam.as </a> unhackable drinking system </h1>
        <p> You can never pass our perfect security </p>
     <form action="login" method="post">
      <div class="container">
        <label for="username"><b>Username</b></label>
        <input type="text" placeholder="Enter Username" name="username" required>

        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>

        <button type="submit">Login</button>
        <label>
          <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
      </div>

      <div class="container" style="background-color:#f1f1f1">
        <button type="button" class="cancelbtn">Cancel</button>
        <span class="password">Forgot <a href="#">password?</a></span>
      </div>
    </form>
    <script>window.addEventListener('resize', () => { fetch("/enhance") }, { once: true });</script>
    """ + spam_notice)
    except Exception as e:
        logger.error(e)
        return str(e), 500


@app.route("/login", methods=["POST"])
def login():
    global drunks
    try:
        attempted_username = flask.request.form["username"]
        attempted_password = flask.request.form["password"]
        if attempted_username == "admin" or attempted_password == "admin":
            drunks += 1
            customer = getOrCreateCustomer(flask.request.remote_addr)
            customer.hacks += 1
            name, n, pronoun = getDrinks(flask.request.remote_addr)
            logger.info(
                name.ljust(16)
                + "| "
                + str(n).ljust(8)
                + "| "
                + flask.request.remote_addr.ljust(16)
                + "| 1"
            )
            if n < 2:
                updateDisplay(name, n, pronoun)
                song = random.choice(os.listdir(f"{sweetTunes}/hacker-tunes"))
                play([f"{sweetTunes}/hacker-tunes/" + song])
                return (
                    "Whaaaat, you haxed us! GZ! We will drink to you " + name
                )
            else:
                updateDisplay(name, n, pronoun)
                return (
                    "Wellcome back "
                    + name
                    + "! You have now haxed us "
                    + str(n)
                    + " times!"
                )
        else:
            return "No hax found, try again"
    except Exception as e:
        logger.error(e)
        return str(e), 500


@app.route("/enhance", methods=["GET"])
def enhance() -> str:
    play([f"{sweetTunes}/enhance.wav"])
    return "enhanced!"


@app.route("/state", methods=["GET"])
def state() -> str:
    """ generate stats html """
    html = """table style="width:100%">
                <tr>
                <th>Name</th>
                <th>H4cks</th>
                <th>Country</th>
              </tr>"""
    for ip, customer in customers.items():
        html += f"""tr>
        <th>{customer.name}</th>
        <th>{customer.hacks}</th>
        <th>{getIpInfo(ip)}</th>
        </tr>"""
    html += "</table>"
    return html


@app.route("/418", methods=["OPTIONS"])
def teapot():
    return "418"


@lru_cache()
def getIpInfo(ip: Ip) -> str:
    url = "http://ip-api.com/json/" + ip + "?fields=countryCode"
    return requests.get(url).json().get("countryCode", "")


def isSpam(ip: Ip) -> bool:
    """ is spam if customer visited less than 60 seconds ago """
    customer = customers[ip]
    return (
        customer.last_visit is not None
        and (datetime.now() - customer.last_visit).total_seconds() < 60
    )


def getUniqueName() -> Tuple[str, str]:
    rand = random.randint(0, 1)
    g = ["male", "female"][rand]
    name = names.get_first_name(gender=g)
    while len(name) > 5:
        name = names.get_first_name()
    return name, ["he", "she"][rand]


def getOrCreateCustomer(ip: Ip) -> Customer:
    global customers
    if ip not in customers:
        name, pronoun = getUniqueName()
        customers[ip] = Customer(
            name=name, hacks=0, pronoun=pronoun, last_visit=None
        )
    return customers[ip]


def countryToDesignator(country: str) -> str:
    try:
        return countryLookupTable[country]
    except Exception:
        return " " + country + "?!!1!?"


def getDrinks(sip: Ip) -> Tuple[str, int, str]:
    customer = getOrCreateCustomer(sip)
    lineOne = customer.name + countryToDesignator(str(getIpInfo(sip)))
    return lineOne, customer.hacks, customer.pronoun


def updateDisplay(lineOne: str, n: int, pronoun: str) -> None:
    """ write info to display """
    if cad is None:
        return
    if n > 1:
        lineTwo = f"Ret for {n} time"
    else:
        lineTwo = pronoun.capitalize() + "'s a new boi!"
    cad.lcd.write(lineOne.ljust(16) + "\n" + lineTwo.ljust(16))
    cad.lcd.home()


def mplayer_play(sound_files: List[str]) -> None:
    """ play sound files with mplayer """
    to_play = " ".join(sound_files)
    output = check_output(f"mplayer {to_play}", shell=True)
    logger.info(f"mplayer return: {output.decode()}")


def play(sound_files: List[str]) -> None:
    """ play sound in new thread """
    thread = threading.Thread(target=mplayer_play, args=(sound_files,))
    thread.start()
