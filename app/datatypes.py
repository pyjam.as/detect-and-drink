from datetime import datetime
from typing import NewType
from dataclasses import dataclass

Ip = NewType("Ip", str)


@dataclass
class Customer:
    name: str
    hacks: int
    pronoun: str
    last_visit: datetime
