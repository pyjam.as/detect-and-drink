FROM python:3.8-slim-buster
# ░█▀█░▀█▀░█▀▀░█▀█░█▀▀░█▀▀░█▀▀░█▀█░█▀▄
# ░█▀▀░░█░░█▀▀░█▀█░█░░░█▀▀░█░░░█▀█░█░█
# ░▀░░░▀▀▀░▀░░░▀░▀░▀▀▀░▀▀▀░▀▀▀░▀░▀░▀▀░
# library for controlling the display on the pi

# install aptitude
RUN apt update
RUN apt install git make mplayer -y

# python-lirc
RUN apt install cython gcc liblircclient-dev python3-dev python3-setuptools -y
RUN git clone https://github.com/tompreston/python-lirc.git
RUN python3 -m pip install cython
WORKDIR python-lirc
RUN make py3 && python3 setup.py install

# pifacecommon
WORKDIR /
RUN git clone https://github.com/piface/pifacecommon.git
WORKDIR pifacecommon
RUN python setup.py install
RUN python3 setup.py install

# pifacecad
WORKDIR /
RUN git clone https://github.com/piface/pifacecad.git
WORKDIR pifacecad/
RUN python setup.py install
RUN python3 setup.py install

# cleanup
WORKDIR /
RUN rm -rf pifacecad/ pifacecommon/ python-lirc/


# python deps
COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

# python app
COPY app/ /app

WORKDIR /app
CMD ["uwsgi", "--http", "0.0.0.0:80", "--callable", "app", "--wsgi-file", "app.py"]
