import sys
import time
import paho.mqtt.client as mqtt

broker_address = "151.216.10.60"
client = mqtt.Client("P1")  # create new instance
client.connect(broker_address)  # connect to broker

# Set color based on script argument
if sys.argv[1] == "ci_start":
    color = "- 0 255"
elif sys.argv[1] == "success":
    color = "0 255 0"
elif sys.argv[1] == "failure":
    color = "255 0 0"

for offset in range(0, 150, 5):
    payload = ""

    # A payload can only be 5 leds
    for i in range(5):
        led_num = offset + i
        payload += f"FYRTAARN {led_num} {color};"

    client.publish("pyjam.as", payload)
