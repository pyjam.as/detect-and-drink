# detect-and-drink

... is a honeypot developed at bornhack 2020

Depending on how -- and from where -- the honeypot is compromised, it will play a sound and you should drink.

Rules are best decided while drunk!