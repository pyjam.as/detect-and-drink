#!/bin/sh
sed -i '$d' /etc/tor/torrc
sed -i -e "\$aExitNodes {$1}" /etc/tor/torrc
systemctl restart tor
sleep 0.1
torsocks curl $2
# Useage example: sudo ./test-country.sh NL http://151.216.10.55/